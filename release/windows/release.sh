#!/bin/sh

set -e

cd $(dirname $0)

TARGET=$1
BUILDDIR=build${TARGET:+-$TARGET}
VERSION=$(cat ../../VERSION)

INSTALLER=hideez-bridge-install.exe

cd ../../$BUILDDIR

cp ../release/windows/hideez-bridge.nsis hideez-bridge.nsis
cp ../release/windows/libs/* .

mingw-strip *.dll *.exe

SIGNKEY=../release/windows/authenticode

if [ -r $SIGNKEY.der ]; then
    mv hideez-bridge.exe hideez-bridge.exe.unsigned
    osslsigncode sign -certs $SIGNKEY.p7b -key $SIGNKEY.der -n "Hideez Bridge" -i "https://hideez.com/" -t http://timestamp.comodoca.com -in trezord.exe.unsigned -out hideez-bridge.exe
    osslsigncode verify -in hideez-bridge.exe
fi

if [ $TARGET = win32 ]; then
    makensis -X"OutFile $INSTALLER" -X'InstallDir "$PROGRAMFILES32\Hideez Bridge"' hideez-bridge.nsis
else
    makensis -X"OutFile $INSTALLER" -X'InstallDir "$PROGRAMFILES64\Hideez Bridge"' hideez-bridge.nsis
fi

if [ -r $SIGNKEY.der ]; then
    mv $INSTALLER $INSTALLER.unsigned
    osslsigncode sign -certs $SIGNKEY.p7b -key $SIGNKEY.der -n "Hideez Bridge" -i "https://hideez.com/" -t http://timestamp.comodoca.com -in $INSTALLER.unsigned -out $INSTALLER
    osslsigncode verify -in $INSTALLER
fi
