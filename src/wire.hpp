/*
 * This file is part of the TREZOR project.
 *
 * Copyright (C) 2014 SatoshiLabs
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef _WIN32
#include <winsock2.h>
#else
#include <netinet/in.h>
#endif

#include <boost/thread.hpp>

#include <cstdint>
#include <string>
#include <vector>
#include <array>

#define READ_TIMEOUT   20000
#define PACKET_TIMEOUT   350

#define WIRE_DEBUG

#ifdef WIRE_DEBUG
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>

extern char wire_log[];

static FILE *wdbgf = NULL;
static bool wdbginit = false;
static void WIREDBG(const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	if (! wdbginit) {
		wdbginit = true;
		wdbgf = fopen(wire_log, "w");
        }
        if (wdbgf) { vfprintf(wdbgf, fmt, ap); fflush(wdbgf); }
	va_end(ap);
}
#else
#define WIREDBG(s...)
#endif

#define IDBYTE(n) (0x80 | ((n) & 0x3f))
#define RESEND    (0xfe)

extern bool isBLE;

static int in_pkt, out_pkt;

namespace trezord
{
namespace wire
{

struct device_info
{
    std::uint16_t vendor_id;
    std::uint16_t product_id;
    std::string path;

    bool
    operator==(device_info const &rhs) const
    {
        return (vendor_id == rhs.vendor_id)
            && (product_id == rhs.product_id)
            && (path == rhs.path);
    }
};

typedef std::vector<device_info> device_info_list;

template <typename F>
device_info_list
enumerate_connected_devices(F filter)
{
    device_info_list list;
    auto *infos = hid::enumerate(0x00, 0x00);

    for (auto i = infos; i != nullptr; i = i->next) {
        // skip unsupported devices
        if (!filter(i)) {
            continue;
        }
        // skip foreign interfaces
        if (i->interface_number > 0) {
            CLOG(DEBUG, "wire.enumerate") << "skipping, invalid device";
            continue;
        }
        // skip debug interface
        if (i->usage_page == 0xFF01) {
            CLOG(DEBUG, "wire.enumerate") << "skipping, debug interface";
            continue;
        }
        // skip fido interface
        if (i->usage_page == 0xF1D0) {
            CLOG(DEBUG, "wire.enumerate") << "skipping, fido interface";
            continue;
        }
        list.emplace_back(
            device_info{
                i->vendor_id,
                i->product_id,
                i->path});
    }

    hid::free_enumeration(infos);
    return list;
}

struct device
{
    typedef std::uint8_t char_type;
    typedef std::size_t size_type;

    struct open_error
        : public std::runtime_error
    { using std::runtime_error::runtime_error; };

    struct read_error
        : public std::runtime_error
    { using std::runtime_error::runtime_error; };

    struct write_error
        : public std::runtime_error
    { using std::runtime_error::runtime_error; };

    device(device const&) = delete;
    device &operator=(device const&) = delete;

    device(char const *path)
    {
        hid = hid::open_path(path);
        if (!hid) {
            throw open_error("HID device open failed");
        }
        hid_version = try_hid_version();
        if (hid_version <= 0) {
            throw open_error("Unknown HID version");
        }
    }

    ~device() { hid::close(hid); }

    // try writing packet that will be discarded to figure out hid version
    int try_hid_version() {
        int r;
        report_type report;

        if (isBLE) {
	    return 3;
	} else {

            // try version 2
            report.fill(0xFF);
            report[0] = 0x00;
            report[1] = 0x3F;
            r = hid::write(hid, report.data(), 65);
            if (r == 65) {
                return 2;
            }
 
            // try version 1
            report.fill(0xFF);
            report[0] = 0x3F;
            r = hid::write(hid, report.data(), 64);
            if (r == 64) {
                return 1;
            }
	}

        // unknown version
        return 0;
    }

    void
    read_buffered(char_type *data,
                  size_type len)
    {
        for (;;) {
            if (read_buffer.empty()) {
                buffer_report();
            }
            size_type n = read_report_from_buffer(data, len);
            if (n < len) {
                data += n;
                len -= n;
            } else {
                break;
            }
        }
    }

    void
    write(char_type const *data,
          size_type len)
    {
	in_pkt = out_pkt = 0;
	flush_input();
        read_buffer.clear();
        for (;;) {
            size_type n = write_report(data, len);
            if (n < len) {
                data += n;
                len -= n;
            } else {
                break;
            }
        }
    }

private:

    size_type
    read_report_from_buffer(char_type *data,
                            size_type len)
    {
        using namespace std;

	WIREDBG("read_report_from_buffer(%d)\n", len);

        size_type n = min(read_buffer.size(), len);
        auto r1 = read_buffer.begin();
        auto r2 = read_buffer.begin() + n;

        copy(r1, r2, data); // copy to data
        read_buffer.erase(r1, r2); // shift from buffer

        return n;
    }


    void
    flush_input()
    {
	unsigned char data[64];
	int r;

	WIREDBG("flush_input\n");
        do {
            r = hid::read_timeout(hid, data, sizeof(data), 5);
	    if (r > 0) WIREDBG(" r:%d\n", r);
        } while (r > 0);
    }

    void
    buffer_report()
    {
        using namespace std;

        report_type report;
        int i, r;
	int ntry = 3;
	std::uint8_t tmpbuf[21];

	WIREDBG("buffer_report\n");

	int tm = (in_pkt == 0) ? READ_TIMEOUT : PACKET_TIMEOUT;

	for (i=0; i<ntry; i++) {
            r = hid::read_timeout(hid, report.data(), report.size(), tm);
	    if (r < 0) throw read_error("hid device read failed");
	    if (r > 0) break;
	    tmpbuf[0] = tmpbuf[1] = 0;
	    tmpbuf[2] = RESEND;
	    tmpbuf[3] = (in_pkt >> 24) & 0xff;
	    tmpbuf[4] = (in_pkt >> 16) & 0xff;
	    tmpbuf[5] = (in_pkt >> 8) & 0xff;
	    tmpbuf[6] = in_pkt & 0xff;
	    hid::write(hid, tmpbuf, 21);
	    tm = PACKET_TIMEOUT;
        }

        if (r == 0) throw read_error("hid device read timeout");

	WIREDBG(" r=%d [ %02x %02x %02x %02x ... ]\n", r, report[0], report[1], report[2], report[3]);
        int start = 0;
        while (start < r-1 && report[start] == 0) start++;
        if (start == r-1) throw read_error("zero packet received");
        if (report[start] == IDBYTE(in_pkt)) {
          WIREDBG("OK\n");
          start++;
          size_type n = min(static_cast<size_type>(18), static_cast<size_type>(r - start));
          copy(report.begin() + start,
             report.begin() + start + n,
             back_inserter(read_buffer));
          in_pkt++;
        }
    }

    size_type
    write_report(char_type const *data,
                 size_type len)
    {
        using namespace std;

        report_type report;
        report.fill(0x00);

	WIREDBG("write_report(%d)\n", len);

	size_type report_size;
	int offset;

        switch (hid_version) {
            case 1:
	        report_size = 64;
                report[0] = 0x3F;
		offset = 1;
                break;
            case 2:
	        report_size = 65;
                report[0] = 0x00;
                report[1] = 0x3F;
		offset = 2;
                break;
            case 3:
	        report_size = 21;
                report[0] = 0x00;
                report[1] = 0x00;
                report[2] = IDBYTE(out_pkt);
		offset = 3;
                break;
        }

        size_type n = report_size - offset;
	if (n > len) n = len;
	copy(data, data + n, report.begin() + offset);

        int r = hid::write(hid, report.data(), report_size);
        if (r < 0) {
            throw write_error{"HID device write failed"};
        }
        if (r < report_size) {
            throw write_error{"HID device write was insufficient"};
        }
	out_pkt++;
        return n;
    }

    typedef std::vector<char_type> buffer_type;
    typedef std::array<char_type, 65> report_type;

    hid_device *hid;
    buffer_type read_buffer;
    int hid_version;
};

struct message
{
    std::uint16_t id;
    std::vector<std::uint8_t> data;

    typedef device::read_error header_read_error;

    void
    read_from(device &device)
    {
        device::char_type buf[6];
        std::uint32_t size;

        device.read_buffered(buf, 1);
        while (buf[0] != '#') {
            device.read_buffered(buf, 1);
        }

        device.read_buffered(buf, 1);
        if (buf[0] != '#') {
            throw header_read_error{"header bytes are malformed"};
        }

        device.read_buffered(buf, 6);

        id = ntohs((buf[0] << 0) | (buf[1] << 8));
        size = ntohl((buf[2] << 0) | (buf[3] << 8) |
                     (buf[4] << 16) | (buf[5] << 24));

	WIREDBG("read_from: id=%x size=%d\n", id, size);

        // 1MB of the message size treshold
        static const std::uint32_t max_size = 1024 * 1024;
        if (size > max_size) {
            throw header_read_error{"message is too big"};
        }

        data.resize(size);
        device.read_buffered(data.data(), data.size());
    }

    void
    write_to(device &device) const
    {
        std::size_t buf_size = 8 + data.size();
        device::char_type buf[buf_size];

        buf[0] = '#';
        buf[1] = '#';

        std::uint16_t id_ = htons(id);
        buf[2] = (id_ >> 0) & 0xFF;
        buf[3] = (id_ >> 8) & 0xFF;

        std::uint32_t size_ = htonl(data.size());
        buf[4] = (size_ >> 0) & 0xFF;
        buf[5] = (size_ >> 8) & 0xFF;
        buf[6] = (size_ >> 16) & 0xFF;
        buf[7] = (size_ >> 24) & 0xFF;

	WIREDBG("write_to: id=%x size=%d\n", id_, size_);

        std::copy(data.begin(), data.end(), &buf[8]);
        device.write(buf, buf_size);
    }
};

}
}
