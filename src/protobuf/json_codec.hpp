/*
 * This file is part of the TREZOR project.
 *
 * Copyright (C) 2014 SatoshiLabs
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "protobuf/state.hpp"

#include <stdexcept>

#include <json/json.h>

#include <google/protobuf/descriptor.h>
#include <google/protobuf/repeated_field.h>

#define JSON_DEBUG

#ifdef JSON_DEBUG
#include <stdio.h>
#include <stdarg.h>
#define MAXSTR 200

extern char json_log[];

static FILE *dbgf = NULL;
static bool dbginit = false;
static char *jdbg_escape(const char *s)
{
	static char buffer[MAXSTR];
	char *p = buffer;
	while (*s) {
		if (*s >= ' ' && *s <= 0x7f) {
			*p++ = *s;
		} else {
			sprintf(p, "\\x%02x", (*s & 0xff));
			p += 4;
		}
		if (p - buffer >= MAXSTR-5) break;
		s++;
	}
	*p = 0;
	return buffer;
}
static void JDBG(const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	if (! dbginit) {
		dbginit = true;
		dbgf = fopen(json_log, "w");
	}
	if (dbgf) { vfprintf(dbgf, fmt, ap); fflush(dbgf); }
	va_end(ap);
}
#else
#define JDBG(s...)
#endif


namespace trezord
{
namespace protobuf
{

namespace pb = google::protobuf;

int p2j_level;
int j2p_level;
int j2p_index;

struct json_codec
{
    json_codec(state *s)
        : protobuf_state(s)
    {}

    Json::Value
    protobuf_to_typed_json(pb::Message const &msg)
    {
        Json::Value val(Json::objectValue);
	p2j_level = 0;

	JDBG("WIRE:\n");

        val["type"] = msg.GetDescriptor()->name();
        val["message"] = protobuf_to_json(msg);

	JDBG("\n");

        return val;
    }

    pb::Message *
    typed_json_to_protobuf(Json::Value const &val)
    {
        auto name = val["type"];
        auto data = val["message"];

        if (!name.isString()) {
            throw std::invalid_argument("expecting JSON string");
        }

        auto descriptor = protobuf_state->descriptor_pool
            .FindMessageTypeByName(name.asString());
        if (!descriptor) {
            throw std::invalid_argument("unknown message");
        }

	j2p_level = 0;
	JDBG("JSON: %s\n", (char *)name.asString().c_str());

        auto prototype = protobuf_state->message_factory
            .GetPrototype(descriptor);

        pb::Message *msg = prototype->New();
        json_to_protobuf(data, *msg);

	JDBG("\n");

        return msg;
    }

private:

    state *protobuf_state;

    Json::Value
    protobuf_to_json(pb::Message const &msg)
    {
        Json::Value val(Json::objectValue);

        auto md = msg.GetDescriptor();
        auto ref = msg.GetReflection();

	p2j_level++;

        for (int i = 0; i < md->field_count(); i++) {
            auto fd = md->field(i);
            auto fname = fd->name();

            try {
                if (fd->is_repeated()) {
                    val[fname] = serialize_repeated_field(msg, *ref, *fd);
                    // no empty arrays for repeated fields
                    if (val[fname].empty()) {
                        val.removeMember(fname);
                    }
                }
                else if (ref->HasField(msg, fd)) {
                    val[fname] = serialize_single_field(msg, *ref, *fd);
                }
            }
            catch (std::exception const &e) {
                throw std::invalid_argument("error while serializing "
                                            + fd->full_name()
                                            + ", caused by: "
                                            + e.what());
            }
        }

	p2j_level--;
	if (p2j_level == 0) JDBG("\n");
        return val;
    }

    void
    json_to_protobuf(Json::Value const &val,
                     pb::Message &msg)
    {
        if (!val.isObject()) {
            throw std::invalid_argument("expecting JSON object");
        }

	j2p_level++;

        auto md = msg.GetDescriptor();
        auto ref = msg.GetReflection();

        for (int i = 0; i < md->field_count(); i++) {
            auto fd = md->field(i);
            auto fname = fd->name();

            if (!val.isMember(fname)) {
                continue;
            }
            try {
                if (fd->is_repeated()) {
                    ref->ClearField(&msg, fd);
                    parse_repeated_field(msg, *ref, *fd, val[fname]);
                }
                else {
                    parse_single_field(msg, *ref, *fd, val[fname]);
                }
            }
            catch (std::exception const &e) {
                throw std::invalid_argument("error while parsing "
                                            + fd->full_name()
                                            + ", caused by: "
                                            + e.what());
            }
        }

	j2p_level--;

    }

    Json::Value
    serialize_single_field(const pb::Message &msg,
                           const pb::Reflection &ref,
                           const pb::FieldDescriptor &fd)
    {
	char pfx[16] = "               ";
	pfx[p2j_level] = 0;

        switch (fd.type()) {

        case pb::FieldDescriptor::TYPE_DOUBLE:
	{
	    double v = ref.GetDouble(msg, &fd);
	    JDBG("%s%s: %f\n", pfx, fd.name().c_str(), v);
            return v;
	}

        case pb::FieldDescriptor::TYPE_FLOAT:
	{
	    float v = ref.GetFloat(msg, &fd);
	    JDBG("%s%s: %f\n", pfx, fd.name().c_str(), (double)v);
            return v;
	}

        case pb::FieldDescriptor::TYPE_INT64:
        case pb::FieldDescriptor::TYPE_SFIXED64:
        case pb::FieldDescriptor::TYPE_SINT64:
	{
	    google::protobuf::int64 v = ref.GetInt64(msg, &fd);
	    JDBG("%s%s: %lld (%llx)\n", pfx, fd.name().c_str(), v, v);
            return Json::Value::Int64(v);
	}

        case pb::FieldDescriptor::TYPE_UINT64:
        case pb::FieldDescriptor::TYPE_FIXED64:
	{
	    google::protobuf::uint64 v = ref.GetUInt64(msg, &fd);
	    JDBG("%s%s: %llu (%llx)\n", pfx, fd.name().c_str(), v, v);
            return Json::Value::UInt64(v);
	}

        case pb::FieldDescriptor::TYPE_INT32:
        case pb::FieldDescriptor::TYPE_SFIXED32:
        case pb::FieldDescriptor::TYPE_SINT32:
	{
	    google::protobuf::int32 v = ref.GetInt32(msg, &fd);
	    JDBG("%s%s: %ld (%lx)\n", pfx, fd.name().c_str(), v, v);
            return v;
	}

        case pb::FieldDescriptor::TYPE_UINT32:
        case pb::FieldDescriptor::TYPE_FIXED32:
	{
	    google::protobuf::uint32 v = ref.GetUInt32(msg, &fd);
	    JDBG("%s%s: %lu (%lx)\n", pfx, fd.name().c_str(), v, v);
            return v;
	}

        case pb::FieldDescriptor::TYPE_BOOL:
	{
	    bool v = ref.GetBool(msg, &fd);
	    JDBG("%s%s: %s\n", pfx, fd.name().c_str(), v ? "true" : "false");
            return v;
	}

        case pb::FieldDescriptor::TYPE_STRING:
	{
	    std::string v = ref.GetString(msg, &fd);
	    JDBG("%s%s: \"%s\"\n", pfx, fd.name().c_str(), jdbg_escape(v.c_str()));
            return v;
	}

        case pb::FieldDescriptor::TYPE_BYTES:
	{
	    std::string v = ref.GetString(msg, &fd);
	    JDBG("%s%s: [%s]\n", pfx, fd.name().c_str(), utils::hex_encode(v).c_str());
            return utils::hex_encode(v);
	}

        case pb::FieldDescriptor::TYPE_ENUM:
	{
	    const std::string v = ref.GetEnum(msg, &fd)->name();
	    JDBG("%s%s: %s\n", pfx, fd.name().c_str(), v.c_str());
            return v;
	}

        case pb::FieldDescriptor::TYPE_MESSAGE:
	{
	    JDBG("%s%s Message:\n", pfx, fd.name().c_str());
            return protobuf_to_json(ref.GetMessage(msg, &fd));
	}

        default:
            throw std::invalid_argument("field of unsupported type");
        }
    }

    Json::Value
    serialize_repeated_field(const pb::Message &msg,
                             const pb::Reflection &ref,
                             const pb::FieldDescriptor &fd)
    {
        Json::Value result(Json::arrayValue);
        int field_size = ref.FieldSize(msg, &fd);
        result.resize(field_size);

        for (int i = 0; i < field_size; i++) {
            result[i] = serialize_repeated_field_item(msg, ref, fd, i);
        }

        return result;
    }

    Json::Value
    serialize_repeated_field_item(const pb::Message &msg,
                                  const pb::Reflection &ref,
                                  const pb::FieldDescriptor &fd,
                                  int i)
    {
	char pfx[16] = "               ";
	pfx[p2j_level] = 0;

        switch (fd.type()) {

        case pb::FieldDescriptor::TYPE_DOUBLE:
	{
	    double v = ref.GetRepeatedDouble(msg, &fd, i);
	    JDBG("%s%s[%d]: %f\n", pfx, fd.name().c_str(), i, v);
            return v;
	}

        case pb::FieldDescriptor::TYPE_FLOAT:
	{
	    float v = ref.GetRepeatedFloat(msg, &fd, i);
	    JDBG("%s%s[%d]: %f\n", pfx, fd.name().c_str(), i, (double)v);
            return v;
	}

        case pb::FieldDescriptor::TYPE_INT64:
        case pb::FieldDescriptor::TYPE_SFIXED64:
        case pb::FieldDescriptor::TYPE_SINT64:
	{
	    google::protobuf::int64 v = ref.GetRepeatedInt64(msg, &fd, i);
	    JDBG("%s%s[%d]: %lld (%llx)\n", pfx, fd.name().c_str(), i, v, v);
            return Json::Value::Int64(v);
	}

        case pb::FieldDescriptor::TYPE_UINT64:
        case pb::FieldDescriptor::TYPE_FIXED64:
	{
	    google::protobuf::uint64 v = ref.GetRepeatedUInt64(msg, &fd, i);
	    JDBG("%s%s[%d]: %llu (%llx)\n", pfx, fd.name().c_str(), i, v, v);
            return Json::Value::UInt64(v);
	}

        case pb::FieldDescriptor::TYPE_INT32:
        case pb::FieldDescriptor::TYPE_SFIXED32:
        case pb::FieldDescriptor::TYPE_SINT32:
	{
	    google::protobuf::int32 v = ref.GetRepeatedInt32(msg, &fd, i);
	    JDBG("%s%s[%d]: %ld (%lx)\n", pfx, fd.name().c_str(), i, v, v);
            return v;
	}

        case pb::FieldDescriptor::TYPE_UINT32:
        case pb::FieldDescriptor::TYPE_FIXED32:
	{
	    google::protobuf::uint32 v = ref.GetRepeatedUInt32(msg, &fd, i);
	    JDBG("%s%s[%d]: %lu (%lx)\n", pfx, fd.name().c_str(), i, v, v);
            return v;
	}

        case pb::FieldDescriptor::TYPE_BOOL:
	{
	    bool v = ref.GetRepeatedBool(msg, &fd, i);
	    JDBG("%s%s[d]: %s\n", pfx, fd.name().c_str(), i, v ? "true" : "false");
            return v;
	}

        case pb::FieldDescriptor::TYPE_STRING:
	{
	    std::string v = ref.GetRepeatedString(msg, &fd, i);
	    JDBG("%s%s[%d]: \"%s\"\n", pfx, fd.name().c_str(), i, jdbg_escape(v.c_str()));
            return v;
	}

        case pb::FieldDescriptor::TYPE_BYTES:
	{
	    std::string v = ref.GetRepeatedString(msg, &fd, i);
	    JDBG("%s%s[%d]: [%s]\n", pfx, fd.name().c_str(), i, utils::hex_encode(v).c_str());
            return utils::hex_encode(v);
	}

        case pb::FieldDescriptor::TYPE_ENUM:
	{
            const std::string v = ref.GetRepeatedEnum(msg, &fd, i)->name();
	    JDBG("%s%s[%d]: %s\n", pfx, fd.name().c_str(), i, v.c_str());
            return v;
	}

        case pb::FieldDescriptor::TYPE_MESSAGE:
	{
	    JDBG("%s%s[%d] Message:\n", pfx, fd.name().c_str(), i);
            return protobuf_to_json(ref.GetRepeatedMessage(msg, &fd, i));
	}

        default:
            throw std::invalid_argument("field of unsupported type");
        }
    }

    void
    parse_single_field(pb::Message &msg,
                       const pb::Reflection &ref,
                       const pb::FieldDescriptor &fd,
                       const Json::Value &val)
    {
	char pfx[16] = "               ";
	pfx[j2p_level] = 0;

        switch (fd.type()) {

        case pb::FieldDescriptor::TYPE_DOUBLE:
	    JDBG("%s%s: %f\n", pfx, fd.name().c_str(),  val.asDouble());
            ref.SetDouble(&msg, &fd, val.asDouble());
            break;

        case pb::FieldDescriptor::TYPE_FLOAT:
	    JDBG("%s%s: %f\n", pfx, fd.name().c_str(), (double)val.asFloat());
            ref.SetFloat(&msg, &fd, val.asFloat());
            break;

        case pb::FieldDescriptor::TYPE_INT64:
        case pb::FieldDescriptor::TYPE_SFIXED64:
        case pb::FieldDescriptor::TYPE_SINT64:
	    JDBG("%s%s: %lld (%llx)\n", pfx, fd.name().c_str(), val.asInt64(), val.asInt64());
            ref.SetInt64(&msg, &fd, val.asInt64());
            break;

        case pb::FieldDescriptor::TYPE_UINT64:
        case pb::FieldDescriptor::TYPE_FIXED64:
	    JDBG("%s%s: %llu (%llx)\n", pfx, fd.name().c_str(), val.asUInt64(), val.asUInt64());
            ref.SetUInt64(&msg, &fd, val.asUInt64());
            break;

        case pb::FieldDescriptor::TYPE_INT32:
        case pb::FieldDescriptor::TYPE_SFIXED32:
        case pb::FieldDescriptor::TYPE_SINT32:
	    JDBG("%s%s: %ld (%lx)\n", pfx, fd.name().c_str(), val.asInt(), val.asInt());
            ref.SetInt32(&msg, &fd, val.asInt());
            break;

        case pb::FieldDescriptor::TYPE_UINT32:
        case pb::FieldDescriptor::TYPE_FIXED32:
	    JDBG("%s%s: %lu (%lx)\n", pfx, fd.name().c_str(), val.asUInt(), val.asUInt());
            ref.SetUInt32(&msg, &fd, val.asUInt());
            break;

        case pb::FieldDescriptor::TYPE_BOOL:
	    JDBG("%s%s: %s\n", pfx, fd.name().c_str(), val.asBool() ? "true" : "false");
            ref.SetBool(&msg, &fd, val.asBool());
            break;

        case pb::FieldDescriptor::TYPE_STRING:
	    JDBG("%s%s: \"%s\"\n", pfx, fd.name().c_str(), jdbg_escape(val.asString().c_str()));
            ref.SetString(&msg, &fd, val.asString());
            break;

        case pb::FieldDescriptor::TYPE_BYTES:
	    JDBG("%s%s: [%s]\n", pfx, fd.name().c_str(), val.asString().c_str());
            ref.SetString(&msg, &fd, utils::hex_decode(val.asString()));
            break;

        case pb::FieldDescriptor::TYPE_ENUM: {
            auto ed = fd.enum_type();
            auto evd = ed->FindValueByName(val.asString());
            if (!evd) {
                throw std::invalid_argument("unknown enum value");
            }
	    JDBG("%s%s: %s\n", pfx, fd.name().c_str(), val.asString().c_str());
            ref.SetEnum(&msg, &fd, evd);
            break;
        }

        case pb::FieldDescriptor::TYPE_MESSAGE: {
            auto mf = &protobuf_state->message_factory;
            auto fm = ref.MutableMessage(&msg, &fd, mf);
	    JDBG("%s%s: Message\n", pfx, fd.name().c_str());
            json_to_protobuf(val, *fm);
            break;
        }

        default:
            throw std::invalid_argument("field of unsupported type");
        }
    }

    void
    parse_repeated_field(pb::Message &msg,
                         const pb::Reflection &ref,
                         const pb::FieldDescriptor &fd,
                         const Json::Value &val)
    {
        if (!val.isArray()) {
            throw std::invalid_argument("expecting JSON array");
        }
	j2p_index = 0;
        for (auto v: val) {
            parse_repeated_field_item(msg, ref, fd, v);
	    j2p_index++;
        }
    }

    void
    parse_repeated_field_item(pb::Message &msg,
                              const pb::Reflection &ref,
                              const pb::FieldDescriptor &fd,
                              const Json::Value &val)
    {
	char pfx[16] = "               ";
	pfx[j2p_level] = 0;
	int i = j2p_index;

        switch (fd.type()) {

        case pb::FieldDescriptor::TYPE_DOUBLE:
	    JDBG("%s%s[%d]: %f\n", pfx, fd.name().c_str(), i, val.asDouble());
            ref.AddDouble(&msg, &fd, val.asDouble());
            break;

        case pb::FieldDescriptor::TYPE_FLOAT:
	    JDBG("%s%s[%d]: %f\n", pfx, fd.name().c_str(), i, (double)val.asFloat());
            ref.AddFloat(&msg, &fd, val.asFloat());
            break;

        case pb::FieldDescriptor::TYPE_INT64:
        case pb::FieldDescriptor::TYPE_SFIXED64:
        case pb::FieldDescriptor::TYPE_SINT64:
	    JDBG("%s%s[%d]: %lld (%llx)\n", pfx, fd.name().c_str(), i, val.asInt64(), val.asInt64());
            ref.AddInt64(&msg, &fd, val.asInt64());
            break;

        case pb::FieldDescriptor::TYPE_UINT64:
        case pb::FieldDescriptor::TYPE_FIXED64:
	    JDBG("%s%s[%d]: %llu (%llx)\n", pfx, fd.name().c_str(), i, val.asUInt64(), val.asUInt64());
            ref.AddUInt64(&msg, &fd, val.asUInt64());
            break;

        case pb::FieldDescriptor::TYPE_INT32:
        case pb::FieldDescriptor::TYPE_SFIXED32:
        case pb::FieldDescriptor::TYPE_SINT32:
	    JDBG("%s%s[%d]: %ld (%lx)\n", pfx, fd.name().c_str(), i, val.asInt(), val.asInt());
            ref.AddInt32(&msg, &fd, val.asInt());
            break;

        case pb::FieldDescriptor::TYPE_UINT32:
        case pb::FieldDescriptor::TYPE_FIXED32:
	    JDBG("%s%s[%d]: %lu (%lx)\n", pfx, fd.name().c_str(), i, val.asUInt(), val.asUInt());
            ref.AddUInt32(&msg, &fd, val.asUInt());
            break;

        case pb::FieldDescriptor::TYPE_BOOL:
	    JDBG("%s%s[%d]: %s\n", pfx, fd.name().c_str(), i, val.asBool() ? "true" : "false");
            ref.AddBool(&msg, &fd, val.asBool());
            break;

        case pb::FieldDescriptor::TYPE_STRING:
	    JDBG("%s%s[%d]: \"%s\"\n", pfx, fd.name().c_str(), i, jdbg_escape(val.asString().c_str()));
            ref.AddString(&msg, &fd, val.asString());
            break;

        case pb::FieldDescriptor::TYPE_BYTES:
	    JDBG("%s%s[d]: [%s]\n", pfx, fd.name().c_str(), i, val.asString().c_str());
            ref.AddString(&msg, &fd, utils::hex_decode(val.asString()));
            break;

        case pb::FieldDescriptor::TYPE_ENUM: {
            auto ed = fd.enum_type();
            auto evd = ed->FindValueByName(val.asString());
            if (!evd) {
                throw std::invalid_argument("unknown enum value");
            }
	    JDBG("%s%s[d]: %s\n", pfx, fd.name().c_str(), i, val.asString().c_str());
            ref.AddEnum(&msg, &fd, evd);
            break;
        }

        case pb::FieldDescriptor::TYPE_MESSAGE: {
            auto mf = &protobuf_state->message_factory;
            auto fm = ref.AddMessage(&msg, &fd, mf);
	    JDBG("%s%s[%d]: Message\n", pfx, fd.name().c_str(), i);
            json_to_protobuf(val, *fm);
            break;
        }

        default:
            throw std::invalid_argument("field of unsupported type");
        }
    }
};

}
}
